# CS3113_SP22_LAB02_JOE



## Tentative Plan
Our plan is to spread the work out as evenly as possible and get as many people covering the lab as possible. To do so we will need to split the work so that there is an 'expert' of the specific objective. Our idea is to use a double linked list to hold all the data about the individual proccesses in the schedulers, and to use the switch-cases to read in the parameters of the generators from the command line. After that whoever is most able can help with finding memory leaks and helping to get 100% code coverage.

## Statement of Work
Our first goal is to try and split the work so that we can maximize the amount of time we have to work on each of the lab sections. This will also allow an 'expert' on the lab section and any help that is provided will be able to have a plethora of experience to draw from. We all plan to do an equal amount of work for this project. We will all attemp parts of the project in an effort to learn from experience, but will ultimately take the best parts and make it greater than their sum. For the write-up and presentation portion, it'll also be equally split and generally touched on throughout the lab as we make changed and implementations. At the end of the day this lab is a learning experience and we plan to solve this problem to improve ourselves for future projects.

## Meeting Notes
We have had numerous meetings where we debated on the algorithms of the schedulers and what techniques we could possibly use. We have attemped many of the ideas that were presented in the meetings, with various degrees of success, and have advanced further because of it.

## Schedule/Timeline
The group will work on this problem with the time we have available. Most of the group prefers to meet online but can meet on Tuesdays so that we can easily show progress and ideas. We already have a basis for the lab with our midterm so all we really need to do is to check up on one another to make sure there are no problems that one of us have already solved.

## Self-Assessment
Logan: 36 1/3%
Clark: 36 1/3%
Nic:   36 1/3%
I would say that as a team we gave 110% to this project. We worked together well and we split the work evenly. Except for some minor communiction issues the project went extremely smoothly.

## Reflection
Logan - As an individual I felt that I could have done more to help communicate with the rest of my team members. If I could start again I would definitely not wait until a later date to begin the coding section and I would have tried to help more with the code coverage and valgrind.

Nic -  I definetly could have communicated more. I ended up duplicating some work that Clark had already done because of poor communication. If I did this project over I hope that I would start working earlier and more consistently.

Clark - For myself I would say that communicating more and sooner would help clarify exactly what our end goal is and to avoid doing a task someone else is doing. Overall I learned quite a bit about how schedulers work and more about the implementations of linked lists.

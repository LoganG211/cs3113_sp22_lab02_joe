#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>
#define MAX_INPUT_LENGTH 8


struct workItem {
	char name[MAX_INPUT_LENGTH];
	char arrivalTime[MAX_INPUT_LENGTH];
	char runTime[MAX_INPUT_LENGTH];
	int turnaroundTime;
	int responseTime;
	int timeRemaining;
	struct workItem* nextItem;
	struct workItem* prevItem;
};

int main(int numArgs, char* args[]) {
	struct workItem* firstItem = (struct workItem*)malloc(sizeof(struct workItem));
	struct workItem* workingItem;

	FILE* inputFile = fopen(args[1], "r");
	FILE* outputFile = fopen(args[2], "w");
	if(inputFile == NULL) {
		printf("input file not found\n");
		return 0;
	}
	char line[100]; //oversized array. This will hold a line from the input file
	int numItems = 1;
	while ( fgets( line, 100, inputFile ) != NULL) { //grabs every line in the file
		if(!firstItem->nextItem){
			workingItem = firstItem;
			firstItem->nextItem = (struct workItem*)malloc(sizeof(struct workItem));

		} else {
			if(!workingItem->nextItem) workingItem->nextItem = (struct workItem*)malloc(sizeof(struct workItem));
			workingItem->nextItem->prevItem = workingItem;
			workingItem = workingItem->nextItem;
		}
		

		int lineIndex = 0;
		int arrayIndex = 0;
		
		//sets every value in workItem to an array of null chars, printing looks funky without this
		for(int i = 0; i < MAX_INPUT_LENGTH; i++){
			workingItem->name[i] = '\0';
			workingItem->arrivalTime[i] = '\0';
			workingItem->runTime[i] = '\0';
		}


		while(line[lineIndex] != ','){
			workingItem->name[arrayIndex] = line[lineIndex];
			lineIndex++;
			arrayIndex++;
		}

		arrayIndex = 0;
		lineIndex++;

		while(line[lineIndex] != ','){
			workingItem->arrivalTime[arrayIndex] = line[lineIndex];
			lineIndex++;
			arrayIndex++;
		}

		arrayIndex = 0;
		lineIndex++;

		while(line[lineIndex] != '\n'){ 		//When a file is read in the second to last character is a newline. Trimming it now b/c it's annoying when I save to a file
			workingItem->runTime[arrayIndex] = line[lineIndex];
			lineIndex++;
			arrayIndex++;
		}
		workingItem->turnaroundTime = 0;
		workingItem->responseTime = -1;
		workingItem->timeRemaining = atoi(workingItem->runTime);
		numItems++;
	}
	//Put the scheduling stuff here
	struct workItem* currLowest;
	int currShortestJobTime = atoi(firstItem->runTime);
	int currTurnaroundTime = 0;
	int currResponseTime = 0;
	int segmentRunTime;
	bool lowerFound = false;
	workingItem = firstItem;
	

	while(firstItem){ //while firstITem is not null, that is while there are any workItems left
	segmentRunTime = 1; //This can be changed to whatever number the user wants
	//find lowest arrivalTime
	lowerFound = false;
	currLowest = firstItem; //starts with the arrival time of the head
	workingItem = firstItem;
	while(workingItem){//goes through all the workItems
		if(workingItem->timeRemaining <= currLowest->timeRemaining && atoi(workingItem->arrivalTime) <= currTurnaroundTime){
			//if a workItem is found which has a lower timeRemaining than currLowest and a arrivalTime <= currTurnaroundTime, then that becomes the new
			//currLowest workItem. Also, lowerFound is set to true, so the item will actually get worked on.
			currShortestJobTime = atoi(workingItem->runTime);
			currLowest = workingItem;
			lowerFound = true;
		}
		workingItem = workingItem->nextItem;
	}
	if(lowerFound){ 
		if(currLowest->responseTime == -1) currLowest->responseTime = currResponseTime;
	while(segmentRunTime != 0){
	if(currLowest->timeRemaining > segmentRunTime){
		currResponseTime += segmentRunTime;
		currLowest->timeRemaining -= segmentRunTime;
		currTurnaroundTime += segmentRunTime;
		segmentRunTime = 0;
	} else {
		currTurnaroundTime += segmentRunTime;
		currLowest->timeRemaining = 0;
		currLowest->turnaroundTime = currTurnaroundTime;
		segmentRunTime = 0;
		fprintf(outputFile, "%s,%s,%s,%i,%i\n", currLowest->name, currLowest->arrivalTime, currLowest->runTime, currLowest->turnaroundTime, currLowest->responseTime);
		if(currLowest->prevItem){
			//if currLowest has a previous and next item
			if(currLowest->nextItem){
				struct workItem* temp = currLowest->prevItem->nextItem;
				currLowest->prevItem->nextItem = currLowest->nextItem;
				currLowest->nextItem->prevItem = currLowest->prevItem;
				free(temp);
			} else { //IE, thelast item
				struct workItem* temp = currLowest->prevItem->nextItem;
				currLowest->prevItem->nextItem = NULL;
				free(temp);
			}
		} else { //IE, the first item
			if(!currLowest->nextItem){ //is this the last item?
				firstItem = NULL;
			} else {
				struct workItem* temp = currLowest->nextItem->prevItem;
				currLowest->nextItem->prevItem = NULL;
				firstItem = currLowest->nextItem;
				free(temp);
			}
		}

		}
	}
	} else {
		//printf("lowernotfound\n");
		//fflush(stdout);
		currTurnaroundTime++;
	}

	}
	free(firstItem);
	free(workingItem);
	free(currLowest);
	fclose(inputFile);
	fclose(outputFile);
	fclose(stdout);
	fclose(stdin);
	fclose(stderr);
	return 0;
}

to use this make file enter [make run-tests-coverage] (without brackets)  on the terminal. This will give code coverage. Currently we have 95~97% coverage from all 7 programs.
to check memory leaks, enter [make run-tests-leak]
type [make clean] to get rid of the gcov files

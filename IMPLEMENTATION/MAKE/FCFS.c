#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#define MAX_INPUT_LENGTH 8


struct workItem {
	char name[MAX_INPUT_LENGTH];
	char arrivalTime[MAX_INPUT_LENGTH];
	char runTime[MAX_INPUT_LENGTH];
	char turnaroundTime[MAX_INPUT_LENGTH];
	char responseTime[MAX_INPUT_LENGTH];
	struct workItem* nextItem;
	struct workItem* prevItem;
};

int main(int numArgs, char* args[]) {
	struct workItem* firstItem = (struct workItem*)malloc(sizeof(struct workItem));
	struct workItem* workingItem;
	FILE* inputFile = fopen(args[1], "r");
	FILE* outputFile = fopen(args[2], "w");
	if(inputFile == NULL) {
		printf("input file not found\n");
		return 0;
	}
	char line[100]; //oversized array. This will hold a line from the input file
	int numItems = 1;
	while ( fgets( line, 100, inputFile ) != NULL) { //grabs every line in the file
		if(!firstItem->nextItem){
			workingItem = firstItem;
			firstItem->nextItem = (struct workItem*)malloc(sizeof(struct workItem));

		} else {
			if(!workingItem->nextItem)workingItem->nextItem = (struct workItem*)malloc(sizeof(struct workItem));
			workingItem->nextItem->prevItem = workingItem;
			workingItem = workingItem->nextItem;
		}
		

		int lineIndex = 0;
		int arrayIndex = 0;
		
		//sets every value in workItem to an array of null chars, printing looks funky without this
		for(int i = 0; i < MAX_INPUT_LENGTH; i++){
			workingItem->name[i] = '\0';
			workingItem->arrivalTime[i] = '\0';
			workingItem->runTime[i] = '\0';
			workingItem->turnaroundTime[i] = '\0';
			workingItem->responseTime[i] = '\0';
		}


		while(line[lineIndex] != ','){
			workingItem->name[arrayIndex] = line[lineIndex];
			lineIndex++;
			arrayIndex++;
		}

		arrayIndex = 0;
		lineIndex++;

		while(line[lineIndex] != ','){
			workingItem->arrivalTime[arrayIndex] = line[lineIndex];
			lineIndex++;
			arrayIndex++;
		}

		arrayIndex = 0;
		lineIndex++;


		while(line[lineIndex] != '\n'){ 		//When a file is read in the second to last character is a newline. Trimming it now b/c it's annoying when I save to a file
			workingItem->runTime[arrayIndex] = line[lineIndex];
			lineIndex++;
			arrayIndex++;
		}
		numItems++;
	}
	//Put the scheduling stuff here
	struct workItem* currLowest;
	int currTurnaroundTime = 0;
	int currResponseTime = 0;
	workingItem = firstItem;
	

	while(firstItem){  //Terminates  when firstItem is null, so when there are no workItems left
	//find lowest arrivalTime
	currLowest = firstItem; //starts with the arrival time of the head
	workingItem = firstItem->nextItem;
	while(workingItem){ //goes through every workItem in the list
		if(atoi(currLowest->arrivalTime) > atoi(workingItem->arrivalTime)){
			//if a workItem with a lower arrivalTime is found, that workItem is set as the currLowest (arrivalTime)
			currLowest = workingItem;
		}
		workingItem = workingItem->nextItem;
	}
	//The lowest arrivalTime workItem's responseTime and turnaroundTime is updated, then it is printed to the output file
	sprintf(currLowest->responseTime, "%d", currResponseTime);
	currTurnaroundTime += atoi(currLowest->runTime);
	currResponseTime = currTurnaroundTime;
	sprintf(currLowest->turnaroundTime, "%d", currTurnaroundTime);
	fprintf(outputFile, "%s,%s,%s,%s,%s\n", currLowest->name, currLowest->arrivalTime, currLowest->runTime, currLowest->turnaroundTime, currLowest->responseTime);
	if(currLowest->prevItem){
		//if currLowest has a previous and next item
		if(currLowest->nextItem){
			struct workItem* temp = currLowest->prevItem->nextItem;
			currLowest->prevItem->nextItem = currLowest->nextItem;
			currLowest->nextItem->prevItem = currLowest->prevItem;
		
			free(temp);
		} else { //IE, the last item in the list
			struct workItem* temp = currLowest->prevItem->nextItem;
			currLowest->prevItem->nextItem = NULL;
		
			free(temp);
		}
	} else { //IE, the first item
		if(!currLowest->nextItem){ //is this the last item?
			firstItem = NULL;
		} else {
			struct workItem* temp = currLowest->nextItem->prevItem;
			currLowest->nextItem->prevItem = NULL;
			firstItem = currLowest->nextItem;
			free(temp);
		}
	}

	}
	free(firstItem);
	free(workingItem);
	free(currLowest);
	fclose(inputFile);
	fclose(outputFile);
	fclose(stdout);
	fclose(stdin);
	fclose(stderr);
	return 0;
}

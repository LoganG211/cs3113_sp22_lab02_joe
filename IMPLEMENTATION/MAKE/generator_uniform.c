#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>

long rand_vald(int seed);

double unifd(int min, int max);

int main(int argc, char *argv[]) {
    int c;
    FILE *output_file;
    int job_num = 0;
    int seed_num = 0;
    int arrival_min = 0;
    int arrival_max = 0;
    int runtime_min = 0;
    int runtime_max = 0;

    while(1){
        int option_index = 0;
        static struct option long_options[] = 
        {
            {"workload-file", required_argument, NULL, 0},
            {"numjobs",       required_argument, NULL, 1},
            {"seed",          required_argument, NULL, 2},
            {"min-arrival",   required_argument, NULL, 3},
            {"max-arrival",   required_argument, NULL, 4},
            {"min-runtime",   required_argument, NULL, 5},
            {"max-runtime",   required_argument, NULL, 6},
            {NULL,            0,                 NULL, 0}
        };

        c = getopt_long(argc, argv, "-:abc:d:::", long_options, &option_index);
        if(c==-1){
            break;
        }
        switch (c)
        {
        case 0:
            //printf("The file command is: %s with the chosen file as %s\n", long_options[option_index].name, optarg);
            output_file = fopen(optarg, "w");
            break;
        case 1:
            //printf("The job number is: %s with size %s\n", long_options[option_index].name, optarg);
            job_num = atoi(optarg);
            //printf("Job Number is: %d\n", job_num);
            break;
        case 2:
            //printf("The seed number is: %s with integer %s\n", long_options[option_index].name, optarg);
            seed_num = atoi(optarg);
            //printf("The command is: %s with file %d\n", long_options[option_index].name, seed_num);
            break;
        case 3:
            //printf("The min arrival is: %s with time %s\n", long_options[option_index].name, optarg);
            arrival_min = atoi(optarg);
            //printf("The command is: %s with file %d\n", long_options[option_index].name, arrival_min);
            break;
        case 4:
            //printf("The max arrival is: %s with time %s\n", long_options[option_index].name, optarg);
            arrival_max = atoi(optarg);
            //printf("The command is: %s with file %d\n", long_options[option_index].name, arrival_max);
            break;
        case 5:
            //printf("The min runtime is: %s with value %s\n", long_options[option_index].name, optarg);
            runtime_min = atoi(optarg);
            //printf("The command is: %s with file %d\n", long_options[option_index].name, runtime_min);
            break;
        case 6:
            //printf("The max runtime is: %s with value %s\n", long_options[option_index].name, optarg);
            runtime_max = atoi(optarg);
            //printf("The command is: %s with file %d\n", long_options[option_index].name, runtime_max);
            break;
        default:
            break;
        }
    }
    rand_vald(seed_num);

    for(int i=0; i<job_num; i++){
        int arrival_time = unifd(arrival_min, arrival_max);
        int run_time = unifd(runtime_min, runtime_max);
        fprintf(output_file, "%d, %d, %d\n", i+1, arrival_time, run_time);
        //printf("%d, %d, %d\n", i+1, arrival_time, run_time);
    }
    fclose(output_file);

    return 0;
}

long rand_vald(int seed)
{
    const long  a =      16807;  // Multiplier
    const long  m = 2147483647;  // Modulus
    const long  q =     127773;  // m div a
    const long  r =       2836;  // m mod a
    static long x;               // Random int value
    long        x_div_q;         // x divided by q
    long        x_mod_q;         // x modulo q
    long        x_new;           // New x value

    // Set the seed if argument is non-zero and then return zero
    if (seed > 0)
    {
      x = seed;
      return(0.0);
    }

    // RNG using integer arithmetic
    x_div_q = x / q;
    x_mod_q = x % q;
    x_new = (a * x_mod_q) - (r * x_div_q);
    if (x_new > 0)
      x = x_new;
    else{
      x = x_new + m;
    }

    return(x);
}

double unifd(int min, int max)
{
    int    z;                     // Uniform random integer number
    int    unif_value;            // Computed uniform value to be returned

    // Pull a uniform random integer
    z = rand_vald(0);

    // Compute uniform discrete random variable using inversion method
    unif_value = z % (max - min + 1) + min;

    return(unif_value);
}

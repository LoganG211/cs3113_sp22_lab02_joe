#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#define MAX_INPUT_LENGTH 8


struct workItem {
	char name[MAX_INPUT_LENGTH];
	char arrivalTime[MAX_INPUT_LENGTH];
	char runTime[MAX_INPUT_LENGTH];
	int turnaroundTime;
	int responseTime;
	int timeRemaining;
	struct workItem* nextItem;
	struct workItem* prevItem;
};

int main(int numArgs, char* args[]) {
	struct workItem* firstItem = (struct workItem*)malloc(sizeof(struct workItem));
	struct workItem* workingItem;

	FILE* inputFile = fopen(args[1], "r");
	FILE* outputFile = fopen(args[2], "w");
	if(inputFile == NULL) {
		printf("input file not found\n");
		return 0;
	}
	char line[100]; //oversized array. This will hold a line from the input file
	int numItems = 1;
	while ( fgets( line, 100, inputFile ) != NULL) { //grabs every line in the file
		if(!firstItem->nextItem){
			workingItem = firstItem;
			firstItem->nextItem = (struct workItem*)malloc(sizeof(struct workItem));

		} else {
			if(!workingItem->nextItem) workingItem->nextItem = (struct workItem*)malloc(sizeof(struct workItem));
			workingItem->nextItem->prevItem = workingItem;
			workingItem = workingItem->nextItem;
		}
		

		int lineIndex = 0;
		int arrayIndex = 0;
		
		//sets every value in workItem to an array of null chars, printing looks funky without this
		for(int i = 0; i < MAX_INPUT_LENGTH; i++){
			workingItem->name[i] = '\0';
			workingItem->arrivalTime[i] = '\0';
			workingItem->runTime[i] = '\0';
		}


		while(line[lineIndex] != ','){
			workingItem->name[arrayIndex] = line[lineIndex];
			lineIndex++;
			arrayIndex++;
		}

		arrayIndex = 0;
		lineIndex++;

		while(line[lineIndex] != ','){
			workingItem->arrivalTime[arrayIndex] = line[lineIndex];
			lineIndex++;
			arrayIndex++;
		}

		arrayIndex = 0;
		lineIndex++;


		while(line[lineIndex] != '\n'){ 		//When a file is read in the second to last character is a newline. Trimming it now b/c it's annoying when I save to a file
			workingItem->runTime[arrayIndex] = line[lineIndex];
			lineIndex++;
			arrayIndex++;
		}
		workingItem->turnaroundTime = 0;
		workingItem->responseTime = -1;
		workingItem->timeRemaining = atoi(workingItem->runTime);
		numItems++;
	}
	//Put the scheduling stuff here
//	int currShortestJobTime = atoi(firstItem->runTime);
	int currTurnaroundTime = 0;
	int currResponseTime = 0;
	int segmentRunTime = 1;
	int prevTurnaroundTime = -1;
	struct workItem* looperator = firstItem; //for reasons that I do not understand using workingItem for the while loop instead of looperator results in memory leakage.
while(firstItem){  //While firstItem is not null, so when there are workItems remaining
looperator = firstItem;  //Necessary to free a malloc, otherwise workingItem would be used here.
	if(prevTurnaroundTime == currTurnaroundTime){//checks if any workItem was processed. If they weren't, currTurnaroundTime is updated.
		currTurnaroundTime += segmentRunTime;
	}
	prevTurnaroundTime = currTurnaroundTime;
	while(looperator){//checks if any workItem was processed. If they weren't, currTurnaroundTime is updated.
		workingItem = looperator;
		if(atoi(workingItem->arrivalTime) <= currTurnaroundTime){
			if(workingItem->responseTime == -1) workingItem->responseTime = currResponseTime;  //repsonseTime is set if the workItem hasn't had it set yet
			//If workingItem's timeRemaining is less than segmentRunTime
			if(workingItem->timeRemaining > segmentRunTime){
				//currResponseTime, currTurnaroundTime, and workingItem's timeRemaining is updated;
				currResponseTime += segmentRunTime;
				workingItem->timeRemaining -= segmentRunTime;
				currTurnaroundTime += segmentRunTime;
				
			} else {
				//workingItem is printed to the output file and removed
				currTurnaroundTime += segmentRunTime;
				workingItem->timeRemaining = 0;
				workingItem->turnaroundTime = currTurnaroundTime;
				fprintf(outputFile, "%s,%s,%s,%d,%d\n", workingItem->name, workingItem->arrivalTime, workingItem->runTime, workingItem->turnaroundTime, workingItem->responseTime);
				if(workingItem->prevItem){
					//if currLowest has a previous and next item
					if(workingItem->nextItem)
					{
						struct workItem* temp = workingItem->prevItem->nextItem;
						workingItem->prevItem->nextItem = workingItem->nextItem;
						workingItem->nextItem->prevItem = workingItem->prevItem;
						free(temp);
					} else { //IE, the last item
					struct workItem* temp = workingItem->prevItem->nextItem;	
					workingItem->prevItem->nextItem = NULL;
					free(temp);
					}
				} else { //IE, the first item
					if(!workingItem->nextItem){ //is this the last item?
						firstItem = NULL;

						
					} else {

						struct workItem* temp = workingItem->nextItem->prevItem;
						workingItem->nextItem->prevItem = NULL;
						firstItem = workingItem->nextItem;
						free(temp);
					
					}
				}

			}
		}
		looperator = looperator->nextItem;
	}
}
//workingItem = firstItem;
free(firstItem);
free(workingItem);
free(looperator);
fclose(inputFile);
fclose(outputFile);
fclose(stdin);
fclose(stdout);
fclose(stderr);
return 0;
}

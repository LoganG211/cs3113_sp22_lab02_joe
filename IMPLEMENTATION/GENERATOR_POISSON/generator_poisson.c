#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <math.h>

int    poisson(double x);       // Returns a Poisson random variable
double expon(double x);         // Returns an exponential random variable
double rand_val(int seed);      // Jain's RNG

int main(int argc, char *argv[]) {
    int c;
    FILE *output_file;
    int job_num;
    int seed_num;
    int lambda_arrival;
    int lambda_runtime;

    while(1){
        int option_index = 0;
        static struct option long_options[] = 
        {
            {"workload-file",    required_argument, NULL, 0},
            {"numjobs",          required_argument, NULL, 1},
            {"seed",             required_argument, NULL, 2},
            {"lambda-arrival",   required_argument, NULL, 3},
            {"lambda-runtime",   required_argument, NULL, 4},
            {NULL,               0,                 NULL, 0}
        };

        c = getopt_long(argc, argv, "-:abc:d:::", long_options, &option_index);
        if(c==-1){
            break;
        }
        switch (c)
        {
        case 0:
            //printf("The file command is: %s with the chosen file as %s\n", long_options[option_index].name, optarg);
            output_file = fopen(optarg, "w");
            break;
        case 1:
            //printf("The job number is: %s with size %s\n", long_options[option_index].name, optarg);
            job_num = atoi(optarg);
            //printf("Job Number is: %d\n", job_num);
            break;
        case 2:
            //printf("The seed number is: %s with integer %s\n", long_options[option_index].name, optarg);
            seed_num = atoi(optarg);
            //printf("The command is: %s with file %d\n", long_options[option_index].name, seed_num);
            break;
        case 3:
            //printf("The min arrival is: %s with time %s\n", long_options[option_index].name, optarg);
            lambda_arrival = atoi(optarg);
            //sscanf(optarg, "%lf", suc_arrival);
            //printf("success-arrival: %d\n", lambda_arrival);
            //printf("The command is: %s with file %d\n", long_options[option_index].name, arrival_min);
            break;
        case 4:
            //printf("The max arrival is: %s with time %s\n", long_options[option_index].name, optarg);
            lambda_runtime = atoi(optarg);
            //sscanf(optarg, "%lf", suc_runtime);
            //printf("success-runtime: %d\n", lambda_runtime);
            //printf("The command is: %s with file %d\n", long_options[option_index].name, arrival_max);
            break;
        default:
            break;
        }
    }
    rand_val(seed_num);

    for(int i=0; i<job_num; i++){
        int arrival_time = poisson(1.0 / lambda_arrival);
        int run_time = poisson(1.0 / lambda_runtime);
        fprintf(output_file, "%d, %d, %d\n", i+1, arrival_time, run_time);
        //printf("%d, %d, %d\n", i+1, arrival_time, run_time);
    }
    fclose(output_file);

    return 0;
}

int poisson(double x)
{
  int    poi_value;             // Computed Poisson value to be returned
  double t_sum;                 // Time sum value

  // Loop to generate Poisson values using exponential distribution
  poi_value = 0;
  t_sum = 0.0;
  while(1)
  {
    t_sum = t_sum + expon(x);
    if (t_sum >= 1.0) break;
    poi_value++;
  }

  return(poi_value);
}

double expon(double x)
{
  double z;                     // Uniform random number (0 < z < 1)
  double exp_value;             // Computed exponential value to be returned

  // Pull a uniform random number (0 < z < 1)
  do
  {
    z = rand_val(0);
  }
  while ((z == 0) || (z == 1));

  // Compute exponential random variable using inversion method
  exp_value = -x * log(z);

  return(exp_value);
}

double rand_val(int seed)
{
  const long  a =      16807;  // Multiplier
  const long  m = 2147483647;  // Modulus
  const long  q =     127773;  // m div a
  const long  r =       2836;  // m mod a
  static long x;               // Random int value
  long        x_div_q;         // x divided by q
  long        x_mod_q;         // x modulo q
  long        x_new;           // New x value

  // Set the seed if argument is non-zero and then return zero
  if (seed > 0)
  {
    x = seed;
    return(0.0);
  }

  // RNG using integer arithmetic
  x_div_q = x / q;
  x_mod_q = x % q;
  x_new = (a * x_mod_q) - (r * x_div_q);
  if (x_new > 0)
    x = x_new;
  else
    x = x_new + m;

  // Return a random value between 0.0 and 1.0
  return((double) x / m);
}
